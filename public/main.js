// Lee el formulario, parsea como tablero de gobstones y lo convierte a instrucciones gobstones
function process() {
  const text = document.getElementById('text').value;
  const out = document.getElementById('gs');
  let resut;
  try {
    result = parse(text);
  } catch (e) {
    console.error(e);
    out.value = 'El tablero no es valido: ' + e.message +'. Seguro elegiste un archivo ".gbb"?';
    return;
  }
  const gs = board2gs(result);
  out.value = 'procedure ArmarTablero() {\n'
    + gs.map(s => '  ' + s).join('\n')
    + '\n}\n';
  draw(result);
}

const COLORS_MAP = { Azul: 0, Negro: 1, Rojo: 2, Verde: 3};
const COLORS = ['Azul', 'Negro', 'Rojo', 'Verde'];

// Utilidades para parsear ----------------------------------------------------------------------
// Parsea un tablero en formato GBB, devuelve un { board, start } donde
// - board es una matrix [x][y][4]
// - start es un array de dos elementos [x, y]
function parse(text) {
  lines = text.split('\n').map(s => s.trim()).filter(s => s && s[0] != '#');
  if (lines[0] != 'GBB/1.0') {
    throw new Error('No empiza con "GBB/1.0"');
  }
  if (lines.length < 3) {
    throw new Error('Muy pocas lineas!');
  }
  const [X, Y] = parseLine('size', lines[1])
  const start = parseLine('head', lines[lines.length-1]);
  const cells = lines.splice(2, lines.length - 3);
  const board = makeBoard(X, Y);
  cells.forEach(line => {
    const [x, y, rest] = parseLine('cell', line);
    if (x >= X || y >= Y) { return; }
    if (!board[x][y]) { board[x][y] = makeEmptyCell() };
    let colorTokens = rest.split(' ').filter(x => x);
    for (let i = 0; i < colorTokens.length; i+= 2) {
      const color = colorTokens[i];
      const n = parseInt(colorTokens[i+1]);
      board[x][y][COLORS_MAP[color]] = n;
    }
  })
  return { board, start }
}
// Crea una celda vacia para el tablero
const makeEmptyCell = () => [0, 0, 0, 0];
// Crea un tablero del tamaño dado
const makeBoard = (sizex, sizey) => {
  const board = Array(sizex);
  for (let i =0; i<sizex; i++) board[i] = Array(sizey);
  return board;
}
// Parsea una linea de GBB: siempre empieza por un keyword, luego una coordinada (x y) y
// opcionalmente cualquier otro texto. Devuelve [x, y, resto]
function parseLine(keyword, str) {
  const m = str.match(keyword + ' ([0-9]+) ([0-9]+)\s*(.*)');
  if (!m) { throw new Error(`Linea mal formada: "${str}" (se esperaba un "${keyword}")`); }
  return [parseInt(m[1]), parseInt(m[2]), m[3]];
}

// Utilidades para generar codigo Gobstones -------------------------------------------------------
// Dado un tablero y celda inicial devuelve un array de instrucciones gobstones para dibujarlo
// (Asume que empieza en 0,0)
const concat = (arr, x) => {
  if (Array.isArray(x)) {
    arr.splice(arr.length, 0, ...x);
  } else {
    arr.push(x);
  }
}
// Devuelve un string, repitiendo una instruccion s por n veces
const gsRepeat = (s, n) => n == 1 ? s : `repeat(${n}) { ${s} }`;
// Devuelve un string, para mover n veces en direccion dir
const gsMoverN = (dir, n) => gsRepeat(`Mover(${dir})`, n);
// Devuelve un string, para poner n bolitas de color c
const gsPonerN = (c, n) => gsRepeat(`Poner(${c})`, n);
// Devuelve un array de instrucciones
const gsMoveTo = (x0, y0, x1, y1) => {
  let ret = [];
  if (x0 < x1) {
    ret.push(gsMoverN('Este', x1 - x0));
  } else if (!x1 && x0) {
    ret.push('IrAlBorde(Oeste)');
  } else if (x0 > x1) {
    ret.push(gsMoverN('Oeste', x0 - x1));
  }
  if (y0 < y1) {
    ret.push(gsMoverN('Norte', y1 - y0));
  } else if (!y1 && y0) {
    ret.push('IrAlBorde(Sur)');
  } else if (y0 > y1) {
    ret.push(gsMoverN('Sur', y0 - y1));
  }
  return ret;
}
const gsCell = (cell) => {
  let ret = [];
  for (let c = 0; c < 4; c++) {
    if (cell[c]) {
      ret.push(gsPonerN(COLORS[c], cell[c]));
    }
  }
  return ret;
}
// Dado un tablero devuelve un array de instrucciones gobstones para armarlo
function board2gs({ board, start }) {
  ret = [];
  ret.push(`// Codigo autogenerado. No modificar! Genera un tablero`);
  ret.push(`// Funciona en tablero de ${board.length} filas y ${board[0].length} columnas`);
  ret.push('--------------------------------------------------------------------') ;
  ret.push('IrAlBorde(Sur); IrAlBorde(Oeste)');
  let posx = 0, posy = 0;
  for (let y = 0; y < board.length; y++) {
    for (let x = 0; x < board.length; x++) {
      const cell = board[x][y];
      if (cell) {
        ret.push(`// Ir a celda (${x}, ${y}), poner bolitas ${cell}`)
        concat(ret, gsMoveTo(posx, posy, x, y));
        concat(ret, gsCell(board[x][y]));
        posx = x;
        posy = y;
      }
    }
  }
  ret.push(`// La celda inicial debe ser fila ${start[1]}, columna ${start[0]}`);
  concat(ret, gsMoveTo(posx, posy, start[0], start[1]));
  console.log("GS", ret, start)
  return ret;
}

function boardToPoints(board) {
  const data = [];
  for (let y = 0; y < board.length; y++) {
    for (let x = 0; x < board.length; x++) {
      for (let c = 0; c < 4; c++) {
        if (board[x][y] && board[x][y][c]) {
          data.push({ x, y, c: COLORS[c], n: board[x][y][c]});
        }
      }
    }
  }
  return data;
}

// Funciones de visualizacion
function draw({ board, start }) {
  const X = board.length;
  const Y = board[0].length;
  const boundingBox = document.getElementById("my_dataviz").getBoundingClientRect();
  const propCellWidth = boundingBox.width / X;
  const propCellHeight = boundingBox.height / Y;
  const maxDimCells = Math.max(X, Y);
  const maxSizePx = boundingBox.width;
  const cellSizePx = Math.min(propCellHeight, propCellWidth); //maxSizePx / maxDimCells;
  // set the dimensions and margins of the graph
  var width = cellSizePx * X, //520 - margin.left - margin.right,
    height = cellSizePx * Y; //520 - margin.top - margin.bottom;
  const radiusCell = 0.45; // 0.4 + 0.4 + margins
  const ballMarginCell = (1 - (radiusCell * 2)) / 3;
  const radiusPx = cellSizePx * radiusCell;

  // append the svg object to the body of the page
  d3.selectAll("svg > *").remove();
  var svg = d3.select("#my_dataviz svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    // .attr("transform",
    //       "translate(" + margin.left + "," + margin.top + ")")

          // d3.select('#chart').remove()
  const data = boardToPoints(board);

  // Add the grey background that makes ggplot2 famous
  svg.append("rect")
    .attr("x",0)
    .attr("y",0)
    .attr("height", height)
    .attr("width", width)
    .style("fill", "#EBEBEB")

  // Add X axis
  var x = d3.scaleLinear()
    .domain([0, X])
    .range([0, width])
  // var offsetx = x => 0; //col => col == "Rojo" || col == "Azul" ? 0.3 : 0.7;
  var offsetx = col => col == "Rojo" || col == "Azul" ? 0 : 0.5;
  svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x).tickSize(-height*1.3).ticks(X))
    .select(".domain").remove()
  // Add Y axis
  var y = d3.scaleLinear()
    .domain([0, Y])
    // .range([0, height])
    .range([height, 0])
    .nice()
  var offsety = col => col == "Negro" || col == "Azul" ? 0 : 0.5;
  svg.append("g")
    .call(d3.axisLeft(y).tickSize(-width*1.3).ticks(Y))
    .select(".domain").remove()
  // Customization
  svg.selectAll(".tick line").attr("stroke", "white")

  // bolitas de colores - desde SVG
  svg.append("g").selectAll("ball").data(data).enter().append("image")
      .attr("xlink:href", p => `img/bola${p.c}.svg`)
      .attr("x", p => x(p.x + offsetx(p.c) + ballMarginCell))
      .attr("y", p => y(p.y + offsety(p.c) + ballMarginCell))
      .attr("transform", "translate(0,-" + radiusPx + ")")
      .attr("width", radiusPx)
      .attr("height", radiusPx);

  // cantidad de boltias
  svg.append('g')
    .selectAll("text")
    .data(data)
    .enter()
      .append("text")
        .attr("x", p => x(p.x + offsetx(p.c) + 0.25))
        .attr("y", p => y(p.y + offsety(p.c) + 0.25))
        .attr("alignment-baseline","middle")
        .style("text-anchor", "middle")
        .style("font-size", (cellSizePx / 4) + "px")
        .style("fill", "#ffffff")
        .text(p => p.n)
}


// Funcion para parsear el tablero cada vez que el texto cambia
var timer = 0;
let lastContents = null;
function changed() {
  const val = document.getElementById('text').value;
  if (lastContents == val) { return; }
  lastContents = val;
  if (timer) { clearTimeout(timer); }
  timer = setTimeout(process, 500);
}

